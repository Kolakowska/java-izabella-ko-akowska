package pl.imiajd.kolakowska;


public class Adres
{
    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, String kod_pocztowy)
    {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public Adres(String ulica, int numer_domu, String miasto, String kod_pocztowy)
    {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
		this.numer_mieszkania = -1;
    }

	public String getKod()
	{
		return this.kod_pocztowy;
	}

	public boolean equals(Adres other)
	{
		if(this.kod_pocztowy.equals(other.getKod()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
    public void pokaz()
    {
        System.out.println(kod_pocztowy + " " + miasto);
        System.out.print(ulica + " " + numer_domu + " ");
		if(numer_mieszkania != -1)
		{
			System.out.print(numer_mieszkania);
		}
		System.out.println("");
    }

    private int numer_domu, numer_mieszkania;
	private String ulica, miasto, kod_pocztowy;
}



