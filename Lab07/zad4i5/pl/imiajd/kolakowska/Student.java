package pl.imiajd.kolakowska;

public class Student extends Osoba
{
    public Student(String nazwisko, int rokUrodzenia, String kierunek)
    {
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }

    public String getKierunek()
    {
        return this.kierunek;
    }

    public String to_S()
    {
        return "Nazwisko: " + getNazwisko() + " | rok urodzenia: " + getRokUrodzenia() + " | kierunek: " + getKierunek();
    }

    private String kierunek;

}

