package pl.imiajd.kolakowska;

public class Nauczyciel extends Osoba
{
    public Nauczyciel(String nazwisko, int rokUrodzenia, double pensja)
    {
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }

    public double getPensja()
    {
        return this.pensja;
    }

    public String to_S()
    {
        return "Nazwisko: " + getNazwisko() + " | rok urodzenia: " + getRokUrodzenia() + " | pensja: " + getPensja();
    }

    private double pensja;

}
