package pl.imiajd.kolakowska;

public class Osoba
{
    public Osoba(String nazwisko, int rokUrodzenia)
    {
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }
    
    public String getNazwisko()
    {
        return this.nazwisko;
    }

    public int getRokUrodzenia()
    {
        return this.rokUrodzenia;
    }
    
    public String to_S()
    {
        return "Nazwisko: " + getNazwisko() + " | rok urodzenia: " + getRokUrodzenia();
    }

    private String nazwisko;
    private int rokUrodzenia;
}
