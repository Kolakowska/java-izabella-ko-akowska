import pl.imiajd.kolakowska.*;

public class Zad4
{
    public static void main(String[] args)
    {
        Osoba test = new Osoba("Testowy", 1993);
        Student test2 = new Student("Testowy2", 1990, "Informatyka");
        Nauczyciel test3 = new Nauczyciel("Testowy3", 1988, 743.50);
        System.out.println(test.to_S());
        System.out.println(test2.to_S());
        System.out.println(test3.to_S());
    }
}
