import pl.imiajd.kolakowska.*;
import java.awt.*;

public class Zad6
{
    public static void main(String[] args)
    {
        BetterRectangle x = new BetterRectangle();
		x.setLocation(0, 0);
		x.setSize(5, 5);
    
        System.out.println(x.getPerimeter());
        System.out.println(x.getArea());

    }
}
