import pl.imiajd.kolakowska.*;
import java.awt.*;

public class Zad6
{
    public static void main(String[] args)
    {
        Point a = new Point(10, 10);
        Dimension b = new Dimension(5, 5);
        BetterRectangle x = new BetterRectangle(a, b);
    
        System.out.println(x.getPerimeter());
        System.out.println(x.getArea());

    }
}
