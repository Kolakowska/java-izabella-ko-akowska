package pl.imiajd.kolakowska;

import java.awt.*;

public class BetterRectangle extends java.awt.Rectangle
{
    public BetterRectangle(Point a, Dimension d)
    {
        super(a, d);
    }
    
    public BetterRectangle(int x, int y, int w, int h)
    {
        super(x, y, w, h);
    }
    
    public BetterRectangle(int w, int h)
    {
        super(w, h);
    }
    
    public double getPerimeter()
    {
        return 2 * (super.getWidth() + super.getHeight());
    }
    
    public double getArea()
    {
       return super.getWidth() * super.getHeight(); 
    }
}
