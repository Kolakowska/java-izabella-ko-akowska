import java.util.Scanner;

public class Zad1i
{
    public static void main(String[] args)
	{
    Scanner in  = new Scanner(System.in);
    System.out.println("Podaj ilość liczb: ");
    int ile = in.nextInt();
    int j = 0;
    double suma = 0;
    int silnia = 1;
    while(j < ile)
	{
        j++;
        silnia *= j;	
        System.out.println("Podaj liczbę: ");
        double liczba = in.nextInt();
        suma += ((liczba * Math.pow(-1, j))/silnia);
    }
    System.out.println("Wartość wyrażeń wynosi: " + suma);
	}

}
