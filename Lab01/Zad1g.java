import java.util.Scanner;

public class Zad1g
{
    public static void main(String[] args)
	{
    Scanner in  = new Scanner(System.in);
    System.out.println("Podaj ilość liczb: ");
    int ile = in.nextInt();
    int j = 0;
    int suma = 0;
    int iloczyn = 1;
    while(j < ile)
	{
        j++;
        System.out.println("Podaj liczbę: ");
        int liczba = in.nextInt();
        suma += liczba;
        iloczyn *= liczba;
    }
    System.out.println("Suma liczb wynosi: " + suma);
    System.out.println("Iloczyn liczb wynosi: " + iloczyn);
	}

}
