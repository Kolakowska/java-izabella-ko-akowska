import java.util.*;

public class Zad2_3
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("podaj liczbe naturalna n: ");
        int n = in.nextInt();
        int j = 0;
        int dodatnie = 0;
        int ujemne = 0;
        int zera = 0;
        while(j<n){
            System.out.println("Podaj kolejna liczbe: ");
            int liczba = in.nextInt();
            if( liczba > 0){
                dodatnie += 1;
            }
            else if(liczba < 0){
                ujemne++;
            }
            else if(liczba == 0){
                zera++;
            }
            j++;
        }
        System.out.println("Dodatnie = : " + dodatnie);
        System.out.println("Ujemne = : " + ujemne);
        System.out.println("zera = : " + zera);
}
}

