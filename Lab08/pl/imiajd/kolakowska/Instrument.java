package pl.imiajd.kolakowska;
import java.time.LocalDate;

public abstract class Instrument
{
    private String producent;
    private LocalDate rokProdukcji;


    public Instrument(String producent, LocalDate rokProdukcji){
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public String getProducent(){
        return producent;
    }

    public LocalDate getRokProdukcji(){
        return rokProdukcji;
    }

    public abstract void dzwiek();

    public boolean equals(Instrument other){
        if(this.producent == other.producent && this.rokProdukcji == other.rokProdukcji)
        {
            return true;
        }
        return false;
    }
    
    public String toString(){
        String napis = " ";
        napis += getProducent() + " " + getRokProdukcji();
        return napis;
    }
}

