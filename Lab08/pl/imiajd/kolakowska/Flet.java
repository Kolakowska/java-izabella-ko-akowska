package pl.imiajd.kolakowska;
import java.time.LocalDate;

public class Flet extends Instrument{
    public Flet(String producent, LocalDate rokProdukcji)
    {
        super(producent, rokProdukcji);
    }

    public void dzwiek(){
        System.out.println("fi , fi");
    }
}
