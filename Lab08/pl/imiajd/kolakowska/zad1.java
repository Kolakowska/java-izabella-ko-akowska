import java.util.*;
import java.time.LocalDate;

public class zad1
{
    public static void main(String[] args)
    {
        String[] imiona = new String[2];
        imiona[0] = "Daniel";
        imiona[1] = "Dawid";
        Osoba[] ludzie = new Osoba[2];
        LocalDate x = LocalDate.now();

        ludzie[0] = new Pracownik("Jan Kowalski",true, imiona, x, 40000.0 , x);
       ludzie[1] = new Student("Małgorzata Nowak",true, imiona, x,"aaaa",20.0);
       String[] imona = ludzie[0].getImiona();
       for (String s : imiona) {
           System.out.println(s);
       }


        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
    }
}

abstract class Osoba
{
    public Osoba(String nazwisko,boolean plec,String[] imie, LocalDate dataUrodzenia)
    {
        this.nazwisko = nazwisko;
        this.plec = plec;
        this.dataUrodzenia = dataUrodzenia;
        imiona = new String[imie.length];
        for(int j = 0 ; j < imie.length ; j++){
            imiona[j] = imie[j];
        }

    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    public boolean getPlec()
    {
        return plec;
    }

    public String[] getImiona(){
        return imiona;
    }
    
    public LocalDate getDataURodzenia(){
        return dataUrodzenia;
    }


    private String nazwisko;
    private String[] imiona;
    private LocalDate dataUrodzenia;
    private boolean plec;

}

class Pracownik extends Osoba
{
    public Pracownik(String nazwisko,boolean plec , String[] imiona , LocalDate dataUrodzenia, double pobory,LocalDate dataZatrudnienia)
    {
        super(nazwisko,plec,imiona,dataUrodzenia);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }
    public LocalDate getDataZatrudnienia(){
        return dataZatrudnienia;
    }


    private double pobory;
    private LocalDate dataZatrudnienia;

}
class Student extends Osoba
{
    public Student(String nazwisko,boolean plec, String[] imiona , LocalDate dataUrodzenia, String kierunek,double sredniaOcen)
    {
        super(nazwisko,plec,imiona,dataUrodzenia);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }
    public double getSredniaOcen(){
        return sredniaOcen;
    }
    public void setSredniaOcen(double srednia){
        this.sredniaOcen = srednia;
    }


    public String getOpis()
    {
        return "Kierunek studiów: " + kierunek;
    }

    private String kierunek;
    private double sredniaOcen;
}

