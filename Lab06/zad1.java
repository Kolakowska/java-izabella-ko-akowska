import java.util.*;

public class zad1
{
    public static void main(String[] args)
    {
        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2 = new RachunekBankowy(3000);
        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());
    }
}

class RachunekBankowy{
    private static double rocznaStopaProcentowa;
    private double saldo;
    public RachunekBankowy(double saldo)
    {
        this.saldo = saldo;
    }
    public void obliczMiesieczneOdsetki()
    {
        double odsetki = (saldo * rocznaStopaProcentowa) / 12;
        this.saldo += odsetki;

    }
    public static void setRocznaStopaProcentowa(double stopa)
    {
        rocznaStopaProcentowa = stopa;
    }
    public double getSaldo()
    {
        return saldo;
    }
}


