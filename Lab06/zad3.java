import java.util.*;
import java.time.LocalDate;
import java.time.DayOfWeek;

public class zad3
{
    public static void main(String[] args)
    {
        Pracownik[] personel = new Pracownik[3];

        personel[0] = new Pracownik("Jan Nowak", 76000, 2007, 4, 13);
        personel[1] = new Pracownik("Adam Sandler", 40000, 2008, 12, 2);
        personel[2] = new Pracownik("Maciej Musiał", 3000, 2010, 2, 13);

        for (Pracownik e : personel) {
            e.zwiekszPobory(20);
        }

        for (Pracownik e : personel) {
            System.out.print("nazwisko = " + e.nazwisko() + "\tid = " + e.id());
            System.out.print("\tpobory = " + e.pobory());
            System.out.printf("\tdataZatrudnienia = %tF\n", e.dataZatrudnienia());
        }
        System.out.println();

        int n = Pracownik.getNextId();
        System.out.println("Następny dostępny id = " + n);

    }
}

class Pracownik
{
    public Pracownik(String nazwisko, double pobory, int year, int month, int day)
    {
        this.nazwisko = nazwisko;
        this.pobory = pobory;

        dataZatrudnienia = LocalDate.of(year,month,day);

        id = nextId;
        ++nextId;
    }

    public String nazwisko()
    {
        return nazwisko;
    }

    public double pobory()
    {
        return pobory;
    }

    public LocalDate dataZatrudnienia()
    {
        return dataZatrudnienia;
    }

    public void zwiekszPobory(double procent)
    {
        double podwyżka = pobory * procent / 100;
        pobory += podwyżka;
    }

    public int id()
    {
        return id;
    }

    public void setId()
    {
        id = nextId;
        ++nextId;
    }

    public static int getNextId()
    {
        return nextId;
    }

    private String nazwisko;
    private double pobory;
    private LocalDate dataZatrudnienia;

    private int id;
    private static int nextId = 1;
}

