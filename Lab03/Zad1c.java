import java.util.*;

public class Zad1c
{
    public static void main(String[] args)
    {
        System.out.println("Podaj liczbe z zakresu  od 1 do 100");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if(n < 1 || n > 100)
        {
            System.out.println("Podana liczba nie miesci sie w zakresie");
            System.exit(0);
        }
        int tablica[] = new int[n];
        Random r = new Random();
        for(int j = 0 ; j < tablica.length ;j++){
            tablica[j] = r.nextInt(2000) - 1000;
        }
        for(int i = 0 ; i < tablica.length; i++){
            System.out.print(tablica[i] + " ");
        }
        System.out.println("");
        int max = tablica[0];
        int min = tablica[0];
        for(int j : tablica){
            if(j<min){
                min = j;
            }
            else if(j > max){
                max = j;
            }
        }
        System.out.println("Maksimum: " + max);
        System.out.println("Minimum: " + min);



    }

}

