import java.util.*;

public class Zad1e
{
    public static void main(String[] args)
    {
        System.out.println("Podaj liczbe z zakresu od 1 do 100");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if(n < 1 || n > 100)
        {
            System.out.println("Podana liczba nie miesci sie w zakresie");
            System.exit(0);
        }
        int tablica[] = new int[n];
        Random r = new Random();
        for(int j = 0 ; j < tablica.length ;j++){
            tablica[j] = r.nextInt(2000) - 1000;
        }
        for(int i = 0 ; i < tablica.length; i++){
            System.out.print(tablica[i] + " ");
        }
        System.out.println("");
        int dlugosc = 0;
        int max = 0;
        for(int j : tablica){
            if(j>0){
                dlugosc++;
            }
            if(dlugosc > max){
                max = dlugosc;
            }
            else if(j < 0){
                dlugosc = 0;
                }
            }
        System.out.println("Maks: " + max);



    }

}

