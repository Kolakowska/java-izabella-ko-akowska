import java.util.*;

public class Zad2c
{
    public static void main(String[] args)
    {
        System.out.println("Podaj liczbe z zakresu od 1 do 100");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if(n < 1 || n > 100)
        {
            System.out.println("Podana liczba nie miesci sie w zakresie");
            System.exit(0);
        }
        int tablica[] = new int[n];
        generuj(tablica,n,-999,999);
        for(int i = 0 ; i < tablica.length; i++){
            System.out.print(tablica[i] + " ");
        }
        System.out.println("");
        System.out.println("Maks : " + ileMaksymalnych(tablica));

    }
    public static void generuj(int tab[],int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        for(int i = 0 ; i < n; i++){
            tab[i] = r.nextInt(maxWartosc-minWartosc) + minWartosc;
        }
    }

    public static int ileDodatnich(int tab[]){
        int dodatnie = 0;
        for(int element: tab){
            if(element > 0){
                dodatnie++;
            }
        }
        return dodatnie;
    }

    public static int ileMaksymalnych(int tab[]){
        int maks = tab[0];
        int ile = 0;
        for(int element: tab){
            if(element == maks){
                ile++;
            }
            if(element > maks){
                maks = element;
                ile = 1;
            }
        }
        return ile;
    }

    public static int ileZerowych(int tab[]){
        int zerowe = 0;
        for(int element: tab){
            if(element == 0){
                zerowe++;
            }
        }
        return zerowe;
    }


}

