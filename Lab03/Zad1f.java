import java.util.*;

public class Zad1f
{
    public static void main(String[] args)
    {
        System.out.println("Podaj liczbe z zakresu od 1 do 100");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if(n < 1 || n > 100)
        {
            System.out.println("Podana liczba nie miesci sie w zakresie");
            System.exit(0);
        }
        int tab[] = new int[n];
        Random r = new Random();
        for(int j = 0 ; j < tab.length ;j++){
            tab[j] = r.nextInt(2000) - 1000;
        }
        for(int i = 0 ; i < tab.length; i++){
            System.out.print(tab[i] + " ");
        }
        System.out.println("");
        for(int j = 0 ; j < tab.length; j++){
            if(tab[j] < 0 ){
                tab[j] = -1;
            }
            else if(tab[j] > 0){
                tab[j] = 1;
            }

        }
        for(int i = 0 ; i < tab.length; i++){
            System.out.print(tab[i] + " ");
        }
        System.out.println("");



    }

}

