import java.util.*;

public class Zad1g
{
    public static void main(String[] args)
    {
        System.out.println("Podaj liczbe z zakresu od 1 do 100");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if(n < 1 || n > 100)
        {
            System.out.println("Podana liczba nie miesci sie w zakresie");
            System.exit(0);
        }
        System.out.println("Podaj lewy koniec z zakresu od 1 do n");
        int lewy = in.nextInt();
        System.out.println("Podaj prawy koniec z zakresu od 1 do n");
        int prawy = in.nextInt();
        int tablica[] = new int[n];
        Random r = new Random();
        for(int j = 0 ; j < tablica.length ;j++){
            tablica[j] = r.nextInt(2000) - 1000;
        }
        for(int i = 0 ; i < tablica.length; i++){
            System.out.print(tablica[i] + " ");
        }
        System.out.println("");
        for(int i = lewy ; i < prawy; i++){
            int temp = tablica[i-1];
            tablica[i-1] = tablica[prawy-1];
            tablica[prawy-1] = temp;
            prawy--;
        }

        for(int i = 0 ; i < tablica.length; i++){
            System.out.print(tablica[i] + " ");
        }
        System.out.println("");



    }

}

