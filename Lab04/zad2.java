import java.util.*;
import java.util.regex.*;

public class zad2
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj substr");
        String substr = in.nextLine();
        System.out.println("Podaj napis");
        String napis = in.nextLine();
        System.out.println("Podany substr wystepuje w napisie " + countSubStr(napis,substr) + " razy");

    }

    public static int countSubStr(String str1, String substr){
    int count = 0;
    Pattern p = Pattern.compile(substr);
    Matcher m = p.matcher(str1);
    while(m.find()){
        count+=1;
    }
    return count;

}
}
