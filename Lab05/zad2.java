import java.lang.*;
import java.util.*;

public class zad2
{
    public static void main(String[] args)
    {
        ArrayList<Integer> a = new ArrayList<>(4);
        ArrayList<Integer> b = new ArrayList<>(5);
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
		
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        ArrayList<Integer> lista = merge(a,b);
        for(int j = 0; j < lista.size() ; j ++)
        {
            System.out.print(lista.get(j) + " ");
        }
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a , ArrayList<Integer> b)
    {
        int rozmiar = a.size() + b.size();
        ArrayList<Integer> lista = new ArrayList<>(rozmiar);
        int max;
        int min;
        if (a.size() > b.size()) {
            max = a.size();
            min = b.size();
        }

        else {
            max = b.size();
            min = a.size();
        }
        for(int j = 0; j < max ; j ++)
        {
            if(j<a.size ()){
                lista.add(a.get(j));
        }
            if( j < b.size()){
                lista.add(b.get(j));
        }
        }
    return lista;
   }
}
