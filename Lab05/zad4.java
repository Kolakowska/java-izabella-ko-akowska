import java.lang.*;
import java.util.*;

public class zad4
{
    public static void main(String[] args)
    {
        ArrayList<Integer> a = new ArrayList<>(4);
        ArrayList<Integer> b = new ArrayList<>(5);
        a.add(1);
        a.add(4);
        a.add(9);
		
        a.add(16);
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        ArrayList<Integer> lista = reversed(a);
        for(int j = 0; j < lista.size() ; j ++)
        {
            System.out.print(lista.get(j) + " ");
        }
        lista = reversed(b);
        System.out.println("");
        for(int j = 0; j < lista.size() ; j ++)
        {
            System.out.print(lista.get(j) + " ");
        }
    }

    public static ArrayList<Integer> reversed(ArrayList<Integer> a)
    {
        int rozmiar = a.size();
        ArrayList<Integer> lista = new ArrayList<>(rozmiar);
        for(int j = rozmiar; j > 0 ; j--)
        {
            lista.add(a.get(j - 1));
        }

    return lista;
   }
}
